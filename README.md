The goal was to create a Crimean Tatar keyboard layout that combines the standard English QWERTY and the extended Ukrainian layouts, without the need for key combinations to type Crimean Tatar Latin characters that do not fit within the usual QWERTY keyboard. Such an approach allows for the implementation by engraving, printing, or sticking symbol labels of three layouts: Ukrainian, Crimean Tatar, and English, which will simplify the use of the keyboard for people who do not have touch typing skills.
In addition, the aim also included the possibility of using the layout for all representatives of the Crimean Tatar people, therefore characters of the Latin alphabet of the Crimean Tatar dialect in Romania were added to the key combinations.
Depending on your keyboard layout, the appearance may vary.
Below there are visualizations of the layout for the three most common types of keyboards.

An alternative layout design (referred to as Version B) was also created, which swaps the positions of the letters I and İ. This version is not intended for pre-installation or for physical application to the keyboard, and is solely designed for people who are accustomed to this key placement.

Our web-site (under development): https://keyboard.aveteam.org

Manual: https://gitlab.com/aveteam/crimean-tatar-keyboard-layout-sources/-/raw/main/manual/manual.pdf?ref_type=heads

Wiki: https://gitlab.com/aveteam/crimean-tatar-keyboard-layout-sources/-/wikis/home

Ave Team 2023
