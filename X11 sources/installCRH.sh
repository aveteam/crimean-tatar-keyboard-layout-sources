#!/bin/bash

# Перевірка наявності сервера X11
if command -v xset &>/dev/null; then
    echo "На вашій системі знайдено X11 сервер."

    # Шлях до XML-файлу
    xml_file="/usr/share/X11/xkb/rules/evdev.xml"

    # Перевірка наявності утиліти xmlstarlet та встановлення, якщо не встановлено
    if ! command -v xmlstarlet &>/dev/null; then
        echo "xmlstarlet не встановлено. Встановлюємо..."
        sudo apt-get update
        sudo apt-get install -y xmlstarlet
    else
        echo "xmlstarlet наявний, можу продовжувати інсталяцію."
    fi

    # Перевірка наявності layout у файлі $xml_file
    if ! sudo xmlstarlet sel -t -c "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']" $xml_file | grep -q "<name>crh</name>"; then
        echo "Зміни у файлі $xml_file потрібні."

        # Вставка нового layout у файл $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList" -t elem -n layout_tmp -v "" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp" -t elem -n configItem -v "" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp/configItem" -t elem -n name -v "crh" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp/configItem" -t elem -n shortDescription -v "crh" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp/configItem" -t elem -n description -v "Crimean Tatar" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp/configItem" -t elem -n languageList -v "" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp/configItem/languageList" -t elem -n iso639Id -v "crh" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout_tmp" -t elem -n variantList -v " " $xml_file
        sudo xmlstarlet ed --inplace -r "/xkbConfigRegistry/layoutList/layout_tmp" -v "layout" $xml_file

        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']/variantList" -t elem -n variant_tmp -v "" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']/variantList/variant_tmp" -t elem -n configItem -v "" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']/variantList/variant_tmp/configItem" -t elem -n name -v "crhb" $xml_file
        sudo xmlstarlet ed --inplace -s "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']/variantList/variant_tmp/configItem" -t elem -n description -v "Crimean Tatar (variant B)" $xml_file
        sudo xmlstarlet ed --inplace -r "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']/variantList/variant_tmp" -v "variant" $xml_file

        echo "Зміни внесено до $xml_file."
    else
        echo "Зміни у файлі $xml_file не потрібні."
    fi

    # Копіюємо файл crh до /usr/share/X11/xkb/symbols/
    sudo cp crh /usr/share/X11/xkb/symbols/

    # Виконуємо команду для оновлення налаштувань клавіатури
    sudo dpkg-reconfigure xkb-data

    echo "Необхідно перезавантажити X11 для застосування змін."

    # Перезавантаження X11
    echo "Починаю процедуру перезавантаження X11..."
    if command -v systemctl &>/dev/null; then
        sudo systemctl restart display-manager
    else
        display_manager=$(cat /etc/X11/default-display-manager)
        case "$display_manager" in
            */lightdm) sudo systemctl restart lightdm ;;
            */gdm*) sudo systemctl restart gdm ;;
            */kdm) sudo systemctl restart kdm ;;
            */mdm) sudo systemctl restart mdm ;;
            *)
                echo "Не вдалося визначити відповідний менеджер вікон. Виконую повний перезапуск системи..."
                sudo reboot
                ;;
        esac
    fi
    echo "Процедуру перезавантаження X11 завершено."

else
    echo "X11 сервер не знайдений на системі."
fi
