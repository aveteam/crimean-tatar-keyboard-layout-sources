#!/bin/bash

symbols_file="/usr/share/X11/xkb/symbols/crh"

# Перевірка наявності утиліти xmlstarlet та встановлення, якщо не встановлено
if ! command -v xmlstarlet &>/dev/null; then
    echo "xmlstarlet не встановлено. Встановлюємо..."
    sudo apt-get update
    sudo apt-get install -y xmlstarlet
else
    echo "xmlstarlet наявний, можу продовжувати інсталяцію."
fi

# Перевіряємо наявність файлу
if [ -e "$symbols_file" ]; then
    # Видаляємо файл, якщо він існує
    sudo rm -f "$symbols_file"
    echo "Файл $symbols_file було видалено."
    xml_file="/usr/share/X11/xkb/rules/evdev.xml"
    sudo xmlstarlet ed --inplace -d "/xkbConfigRegistry/layoutList/layout[configItem/name='crh']" "$xml_file"
else
    echo "Файл $symbols_file не існує."
fi

# Виконуємо команду для оновлення налаштувань клавіатури
sudo dpkg-reconfigure xkb-data

echo "Необхідно перезавантажити X11 для застосування змін."

# Перезавантаження X11
echo "Починаю процедуру перезавантаження X11..."
if command -v systemctl &>/dev/null; then
    sudo systemctl restart display-manager
else
    display_manager=$(cat /etc/X11/default-display-manager)
    case "$display_manager" in
        */lightdm) sudo systemctl restart lightdm ;;
        */gdm*) sudo systemctl restart gdm ;;
        */kdm) sudo systemctl restart kdm ;;
        */mdm) sudo systemctl restart mdm ;;
        *)
            echo "Не вдалося визначити відповідний менеджер вікон. Виконую повний перезапуск системи..."
            sudo reboot
            ;;
    esac
fi
echo "Процедуру перезавантаження X11 завершено."
